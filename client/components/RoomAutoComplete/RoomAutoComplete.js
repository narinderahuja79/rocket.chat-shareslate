import { AutoComplete, Option, Box } from '@rocket.chat/fuselage';
import React, { memo, useEffect, useMemo, useState } from 'react';

import { useUser } from '../../contexts/UserContext';
import { useEndpointData } from '../../hooks/useEndpointData';
import RoomAvatar from '../avatar/RoomAvatar';
import Avatar from './Avatar';

const query = (term = '') => ({ selector: JSON.stringify({ name: term }) });

const RoomAutoComplete = (props) => {
	const [filter, setFilter] = useState('');
	const { value: data } = useEndpointData(
		'rooms.autocomplete.channelAndPrivate',
		useMemo(() => query(filter), [filter]),
	);
	const [sortedData, setSortedData] = useState([]);

	console.log('Create discussion chennel list--------', data);

	const user = useUser();
	const userRole = user && user.roles;
	const userName = user && user.username;

	useEffect(() => {
		// if (userRole && userRole.includes('admin')) {
		// const dd = (data && data.map(({ name, _id, avatarETag, t }) => ({
		// 	value: _id,
		// 	label: { name, avatarETag, type: t },
		// }))
		// );
		// setSortedData(dd);
		// } else {
		const newData =
			data &&
			data.items.filter((item) => item.t.toString().toLowerCase().indexOf('p'.toLowerCase()) > -1);
		const tempData =
			newData &&
			newData.map(({ name, _id, avatarETag, t }) => ({
				value: _id,
				label: { name, avatarETag, type: t },
			}));
		setSortedData(tempData);
		console.log('sortedData----', tempData);
		// }
	}, [data, userName, userRole]);

	console.log('user detail-------', user, userName);

	// console.log('data--------', data);

	// const options = useMemo(
	// 	() =>
	// 		(data &&
	// 			data.items.map(({ name, _id, avatarETag, t }) => ({
	// 				value: _id,
	// 				label: { name, avatarETag, type: t },
	// 			}))) ||
	// 		[],
	// 	[data],
	// );

	return (
		<AutoComplete
			{...props}
			filter={filter}
			setFilter={setFilter}
			renderSelected={({ value, label }) => (
				<>
					<Box margin='none' mi='x2'>
						<RoomAvatar size='x20' room={{ type: label?.type || 'c', _id: value, ...label }} />{' '}
					</Box>
					<Box margin='none' mi='x2'>
						{label?.name}
					</Box>
				</>
			)}
			renderItem={({ value, label, ...props }) => (
				<Option
					key={value}
					{...props}
					label={label.name}
					avatar={<Avatar value={value} {...label} />}
				/>
			)}
			options={sortedData}
			// options={options}
		/>
	);
};

export default memo(RoomAutoComplete);
