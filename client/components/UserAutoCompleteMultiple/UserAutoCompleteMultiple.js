import { AutoComplete, Box, Option, Chip } from '@rocket.chat/fuselage';
import { useMutableCallback, useDebouncedValue } from '@rocket.chat/fuselage-hooks';
import React, { memo, useEffect, useMemo, useState } from 'react';

import { useUser } from '../../contexts/UserContext';
import { useEndpointData } from '../../hooks/useEndpointData';
import UserAvatar from '../avatar/UserAvatar';
import Avatar from './Avatar';

let items = [];
const query = (term = '') => ({ selector: JSON.stringify({ term }) });

const UserAutoCompleteMultiple = (props) => {
	const [filter, setFilter] = useState('');
	const debouncedFilter = useDebouncedValue(filter, 1000);
	const { value: data } = useEndpointData(
		'users.autocomplete',
		useMemo(() => query(debouncedFilter), [debouncedFilter]),
	);

	// const { value: items } = useEndpointData(
	// 	'users.autocomplete',
	// 	useMemo(() => query(debouncedFilter), [debouncedFilter]),
	// );

	useEffect(() => {
		items = JSON.parse(localStorage.getItem('items'));
		console.log('items ---------', items);
	}, []);

	// const options = useMemo(
	// 	() => (data && data.items.map((user) => ({ value: user.username, label: user.name }))) || [],
	// 	[data],
	// );
	const onClickRemove = useMutableCallback((e) => {
		e.stopPropagation();
		e.preventDefault();
		props.onChange(e.currentTarget.value, 'remove');
	});

	// let userName = localStorage.getItem('name')

	const [sortedData, setSortedData] = useState([]);
	const user = useUser();
	const userRole = user && user.roles;
	const userName = user && user.username;

	useEffect(() => {
		// if (userRole && userRole.includes('admin')) {
		// 	setSortedData(items);
		// } else {
		const newData = items.filter(
			(item) =>
				// item.t.toString().toLowerCase().indexOf('p'.toLowerCase()) > -1 ||
				item.usernames && item.usernames.includes(userName),
		);
		const tempArray = newData && newData.map((user) => ({ value: user.name, label: user.fname }));
		setSortedData(tempArray);
		console.log('sortedData----', newData);
		console.log('sortedData----', tempArray);
		// }
	}, [userName, userRole]);

	// console.log('items-------', items, user, userName);

	// console.log('options ---------', options);
	console.log('data ---------', data);
	console.log('username ---------', userName);
	// console.log('items ---------', items);

	return (
		<AutoComplete
			{...props}
			filter={filter}
			setFilter={setFilter}
			renderSelected={({ value: selected }) =>
				selected?.map((value) => (
					<Chip key={value} {...props} height='x20' value={value} onClick={onClickRemove} mie='x4'>
						<UserAvatar size='x20' username={value} />
						<Box is='span' margin='none' mis='x4'>
							{value}
						</Box>
					</Chip>
				))
			}
			renderItem={({ value, ...props }) => (
				<Option key={value} {...props} avatar={<Avatar value={value} />} />
			)}
			options={sortedData}
			// options={options}
		/>
	);
};

export default memo(UserAutoCompleteMultiple);
