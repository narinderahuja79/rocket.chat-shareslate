import { Sidebar } from '@rocket.chat/fuselage';
// import { useMutableCallback } from '@rocket.chat/fuselage-hooks';
import React from 'react';

// import { useRoute } from '../../../contexts/RouterContext';
// import { useSetting } from '../../../contexts/SettingsContext';

const HomeMain = () => {
	// const homeRoute = useRoute('https://www.shareslate.com');
	// const showHome = useSetting('Layout_Show_Home_Button');
	// const handleHome = useMutableCallback(() => homeRoute.push({}));

	const handleClick = () => {
		window.location.assign('https://www.shareslate.com/home.php');
	};

	return <Sidebar.TopBar.Action icon='home' onClick={() => handleClick()} />;
};

export default HomeMain;
