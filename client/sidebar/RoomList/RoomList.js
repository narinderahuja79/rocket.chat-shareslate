import { Box } from '@rocket.chat/fuselage';
import { useDebouncedValue, useResizeObserver } from '@rocket.chat/fuselage-hooks';
import { escapeRegExp } from '@rocket.chat/string-helpers';
import React, { useRef, useEffect, useMemo, useState } from 'react';
import { Virtuoso } from 'react-virtuoso';

import { useSession } from '../../contexts/SessionContext';
import { useTranslation } from '../../contexts/TranslationContext';
import {
	useUserPreference,
	useUserId,
	useUser,
	useUserSubscriptions,
} from '../../contexts/UserContext';
import { useAvatarTemplate } from '../hooks/useAvatarTemplate';
import { usePreventDefault } from '../hooks/usePreventDefault';
import { useRoomList } from '../hooks/useRoomList';
import { useShortcutOpenMenu } from '../hooks/useShortcutOpenMenu';
import { useSidebarPaletteColor } from '../hooks/useSidebarPaletteColor';
import { useTemplateByViewMode } from '../hooks/useTemplateByViewMode';
import Row from './Row';
import ScrollerWithCustomProps from './ScrollerWithCustomProps';

const RoomList = () => {
	useSidebarPaletteColor();
	const listRef = useRef();
	const { ref } = useResizeObserver({ debounceDelay: 100 });

	const openedRoom = useSession('openedRoom');

	const sidebarViewMode = useUserPreference('sidebarViewMode');
	const sideBarItemTemplate = useTemplateByViewMode();
	const avatarTemplate = useAvatarTemplate();
	const extended = sidebarViewMode === 'extended';
	const isAnonymous = !useUserId();

	const t = useTranslation();

	const user = useUser();
	const userRole = user && user.roles;
	// console.log('users from Room list----------', user);
	localStorage.setItem('role', user && user.roles);
	localStorage.setItem('name', user && user.name);

	const roomsList = useRoomList();
	// console.log('Room list----------', roomsList);
	const roomIndex = roomsList.findIndex((room) => room.name === 'general');

	const roomFinalList = () => {
		if (userRole && userRole.includes('admin')) {
			return roomsList;
		}
		roomsList.splice(roomIndex, 1);
		return roomsList;
	};

	const useSearchItems = (filterText) => {
		const expression = /(@|#)?(.*)/i;
		const teste = filterText.match(expression);

		const [, type, name] = teste;
		const query = useMemo(() => {
			const filterRegex = new RegExp(escapeRegExp(name), 'i');

			return {
				$or: [{ name: filterRegex }, { fname: filterRegex }],
				...(type && {
					t: type === '@' ? 'd' : { $ne: 'd' },
				}),
			};
		}, [name, type]);

		const localRooms = useUserSubscriptions(query);
		return { data: Array.from(new Set([...localRooms])) };
	};

	const [filter] = useState('');

	const filterText = useDebouncedValue(filter, 100);

	const { data: items } = useSearchItems(filterText);
	localStorage.setItem('items', JSON.stringify(items));

	const itemData = useMemo(
		() => ({
			extended,
			t,
			SideBarItemTemplate: sideBarItemTemplate,
			AvatarTemplate: avatarTemplate,
			openedRoom,
			sidebarViewMode,
			isAnonymous,
			items,
		}),
		[
			avatarTemplate,
			extended,
			isAnonymous,
			openedRoom,
			sideBarItemTemplate,
			sidebarViewMode,
			t,
			items,
		],
	);

	usePreventDefault(ref);
	useShortcutOpenMenu(ref);

	useEffect(() => {
		listRef.current?.resetAfterIndex(0);
	}, [sidebarViewMode]);

	console.log('items data-------', items);

	return (
		<Box h='full' w='full' ref={ref}>
			<Virtuoso
				totalCount={roomsList.length}
				// data={roomsList}
				data={roomFinalList()}
				components={{ Scroller: ScrollerWithCustomProps }}
				itemContent={(index, data) => <Row data={itemData} item={data} />}
			/>
		</Box>
	);
};

export default RoomList;
